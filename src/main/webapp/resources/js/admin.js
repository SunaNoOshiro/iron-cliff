	var serviceToEditId = 0;
		var serviceToDeleteId = 0;



		function openEditServiceModal(id) {
			$.ajax({
				url : "/admin/service/" + id,
				method : "GET"

			}).done(
					function(data) {
						$("#editServiceModal #nameru").val(
								data.serviceDatas['ru'].name);
						$("#editServiceModal #nameua").val(
								data.serviceDatas['ua'].name);
						$("#editServiceModal #nameus").val(
								data.serviceDatas['us'].name);

						$("#editServiceModal #descriptionru").val(
								data.serviceDatas['ru'].description);
						$("#editServiceModal #descriptionua").val(
								data.serviceDatas['ua'].description);
						$("#editServiceModal #descriptionus").val(
								data.serviceDatas['us'].description);

						$("#editServiceModal #shortDescriptionsru").val(
								data.serviceDatas['ru'].shortDescription);
						$("#editServiceModal #shortDescriptionsua").val(
								data.serviceDatas['ua'].shortDescription);
						$("#editServiceModal #shortDescriptionsus").val(
								data.serviceDatas['us'].shortDescription);

						$("#editServiceModal #price").val(data.price);
						$("#editServiceModal #category").val(
								data.category.names['${locale}'].value);

						serviceToEditId = id;

						$("#editServiceModal label").attr("class", "active");
					});
		}

		function openEditReportModal(id) {
			$.ajax({
				url : "/admin/report/" + id,
				method : "GET"

			}).done(
					function(data) {

						$("#editReportModal #customer_ru").val(
								data.reportDatas['ru'].customer);
						$("#editReportModal #customer_ua").val(
								data.reportDatas['ua'].customer);
						$("#editReportModal #customer_us").val(
								data.reportDatas['us'].customer);

						$("#editReportModal #deceasedPerson_ru").val(
								data.reportDatas['ru'].deceasedPerson);
						$("#editReportModal #deceasedPerson_ua").val(
								data.reportDatas['ua'].deceasedPerson);
						$("#editReportModal #deceasedPerson_us").val(
								data.reportDatas['us'].deceasedPerson);

						$("#editReportModal #location_ru").val(
								data.reportDatas['ru'].location);
						$("#editReportModal #location_ua").val(
								data.reportDatas['ua'].location);
						$("#editReportModal #location_us").val(
								data.reportDatas['us'].location);

						$("#editReportModal #cemetery_ru").val(
								data.reportDatas['ru'].cemetery);
						$("#editReportModal #cemetery_ua").val(
								data.reportDatas['ua'].cemetery);
						$("#editReportModal #cemetery_us").val(
								data.reportDatas['us'].cemetery);

						$("#editReportModal #completedWorks_ru").val(
								data.reportDatas['ru'].completedWorks);
						$("#editReportModal #completedWorks_ua").val(
								data.reportDatas['ua'].completedWorks);
						$("#editReportModal #lcompletedWorks_us").val(
								data.reportDatas['us'].completedWorks);

						$("#editReportModal #orderDate").val(data.orderDate);
						$("#editReportModal #dueDate").val(data.dueDate);

						$("#editReportModal label").attr("class", "active");

						reportToEditId = id;
					});
		}

		function saveService() {
			$.ajax({
				url : "/admin/service/" + serviceToEditId,
				type : "PUT",
				data : $("#editServiceModalform").serialize()
			}).done(function() {
				window.location.reload();
			});
		}

		function saveReport() {
			$.ajax({
					url : "/admin/report/" + reportToEditId,
					type : "PUT",
					data : {
						"report" : $("#editReportModalform").serialize(),
						"filesBefore" : "",
						"filesAfter" :
					}
				}
			}).done(function() {
// 				window.location.reload();
			});
		}

		function openDeleteServiceModal(id) {
			$.ajax({
				url : "/admin/service/" + id,
				type : "GET"

			}).done(
					function(data) {
						if (data.serviceDatas['${locale}'] !== undefined
								|| data.serviceDatas['${locale}'] != null) {
							$("#serviceToDelete").html(
									data.serviceDatas['${locale}'].name);
						}

						$("#serviceToDeleteID").html(id);

						serviceToDeleteId = id;
					});

		}

		function openDeleteReportModal(id) {
			$.ajax({
				url : "/admin/report/" + id,
				type : "GET"

			}).done(
					function(data) {
						$("#reportToDelete").html(
								data.reportDatas['${locale}'].customer
										+ data.orderDate);

						$("#reportToDeleteID").html(id);

						reportToDeleteId = id;
					});

		}

		function deleteService() {
			$.ajax({
				url : "/admin/service/" + serviceToDeleteId,
				type : "DELETE"
			}).done(function() {
				window.location.reload();
			});
		}

		function deleteReport() {
			$.ajax({
				url : "/admin/report/" + reportToDeleteId,
				type : "DELETE"
			}).done(function() {
				window.location.reload();
			});
		}