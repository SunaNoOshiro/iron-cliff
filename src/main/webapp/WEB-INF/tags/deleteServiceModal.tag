<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@tag pageEncoding="UTF-8"%>

<div id="deleteServiceModal" class="modal modal-fixed-footer">
	<div class="modal-content">
		<form  method="post" id="deleteServiceModalform">
			<h2>
				<spring:message code="page.admin.form.service.message.deleteService" /> "<span
					id="serviceToDeleteID"></span>:<span id="serviceToDelete"></span>"
			</h2>
			<div class="col s12">
				<button class="modal-action waves-effect waves-light btn"
					style="float: right; margin: 10px;" ><spring:message code="page.admin.form.service.button.cancel" /></button>
				<a href="#"
					class="modal-action modal-close waves-effect waves-light red btn"
					style="float: right; margin: 10px;"><spring:message code="page.admin.form.service.button.delete" /></a>
			</div>
		</form>
	</div>
</div>