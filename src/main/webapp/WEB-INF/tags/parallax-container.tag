<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@tag pageEncoding="UTF-8"%>

<style>
.parallax-container {
    height: 200px;
}
</style>

<div class="parallax-container">
	<div class="parallax">
		<img
			src="/resources/images/obediahdaviscem.jpg">
	</div>
</div>