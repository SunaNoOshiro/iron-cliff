<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@tag pageEncoding="UTF-8"%>

<script type="text/javascript">
	$(document).ready(function() {	
		$(".bordereded").stupidtable();
		$('.modal-trigger').leanModal();
		$('.datepicker').pickadate();
		$('ul.tabs').tabs();
		$('.parallax').parallax();
		$(".dropdown-button").dropdown();
		$('.slider').slider();
		$('select').material_select();
		
		var toolbar = [
		               ['style', ['style', 'bold', 'italic', 'underline', 'strikethrough', 'clear']],
		               ['fonts', ['fontsize', 'fontname']],
		               ['color', ['color']],
		               ['undo', ['undo', 'redo', 'help']],
		               ['ckMedia', ['ckImageUploader', 'ckVideoEmbeeder']],
		               ['misc', ['link', 'picture', 'table', 'hr', 'codeview', 'fullscreen']],
		               ['para', ['ul', 'ol', 'paragraph', 'leftButton', 'centerButton', 'rightButton', 'justifyButton', 'outdentButton', 'indentButton']],
		               ['height', ['lineheight']],
		           ];

		           $('.editor').materialnote({
		               toolbar: toolbar,
		               height: 550,
		               minHeight: 100,
		               defaultBackColor: '#e0e0e0'
		           });
	});
</script>