<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@tag pageEncoding="UTF-8"%>

<footer class="page-footer">
	<div class="container">
		<div class="row">
			<div class="col l6 s12">
				<h5 class="white-text">Footer Content</h5>
				<p class="grey-text text-lighten-4">You can use rows and columns
					here to organize your footer content.</p>
			</div>
			<div class="col l4 offset-l2 s12">
				<h5 class="white-text">Інформація</h5>
				<ul>
					<li><a class="grey-text text-lighten-3" href="#!">Про нас</a></li>
					<li><a class="grey-text text-lighten-3" href="#!">Послуги</a></li>
					<li><a class="grey-text text-lighten-3" href="#!">Кладовища</a></li>
					<li><a class="grey-text text-lighten-3" href="#!">Звіти
							про роботу</a></li>
				</ul>
			</div>
		</div>
	</div>

	<div class="footer-copyright">
		<div class="container">
			© 2014 Copyright Text <a class="grey-text text-lighten-4 right"
				href="#!">More Links</a>
		</div>
	</div>
</footer>