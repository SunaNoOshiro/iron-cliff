<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="custom" tagdir="/WEB-INF/tags"%>
<%@tag pageEncoding="UTF-8"%>


<table class="bordered bordereded highlight" >
	<thead>
		<tr>
			<th data-field="id" data-sort="int"><spring:message code="page.admin.table.report.id" /></th>
			<th data-field="customer" data-sort="string"><spring:message code="page.admin.table.report.customer" /></th>
			<th data-field="deceasedPerson" data-sort="string"><spring:message code="page.admin.table.report.deceasedPerson" /></th>
			<th data-field="orderDate" data-sort="string"><spring:message code="page.admin.table.report.orderDate" /></th>
			<th data-field="dueDate" data-sort="string"><spring:message code="page.admin.table.report.dueDate" /></th>
			<th data-field="location" data-sort="string"><spring:message code="page.admin.table.report.location" /></th>
			<th data-field="cemetery" data-sort="string"><spring:message code="page.admin.table.report.cemetery" /></th>
			<th data-field="edit"></th>
			<th data-field="delete"></th>
		</tr>
	</thead>

	<tbody>
		<c:forEach var="report" items="${reports}">
			<tr>
				<td><c:out value="${report.id}" /></td>
				<td><c:out value="${report.getReportData(dataLocale).customer}" /></td>
				<td><c:out value="${report.getReportData(dataLocale).deceasedPerson}" /></td>
				<td><c:out value="${report.orderDate}" /></td>
				<td><c:out value="${report.dueDate}" /></td>
				<td><c:out value="${report.getReportData(dataLocale).location}" /></td>
				<td><c:out value="${report.getReportData(dataLocale).cemetery}" /></td>

				<td><a class="modal-trigger" href="#editReportModal"
					onclick="openEditReportModal(${report.id});"><i
						class="material-icons">mode_edit</i></a></td>
				<td><a class="modal-trigger" href="#deleteReportModal"
					onclick="openDeleteReportModal(${report.id});"><i
						class="material-icons">delete</i></a></td>
			</tr>
		</c:forEach>
	</tbody>
</table>
