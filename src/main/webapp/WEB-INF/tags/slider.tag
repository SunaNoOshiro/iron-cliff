<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@tag pageEncoding="UTF-8"%>
<div class="slider">
	<ul class="slides">
		<li><img src="http://lorempixel.com/580/250/nature/1"> <!-- random image -->
			<div class="caption right-align">
				<h3>Уход за могилами</h3>
				<h5 class="light grey-text text-lighten-3">Here's our small
					slogan.</h5>
			</div></li>
		<li><img src="http://lorempixel.com/580/250/nature/2"> <!-- random image -->
			<div class="caption left-align">
				<h3>Реставрация могил и памятников</h3>
				<h5 class="light grey-text text-lighten-3">Here's our small
					slogan.</h5>
			</div></li>
		<li><img src="http://lorempixel.com/580/250/nature/3"> <!-- random image -->
			<div class="caption right-align">
				<h3>Возложение цветов и озеленение</h3>
				<h5 class="light grey-text text-lighten-3">Here's our small
					slogan.</h5>
			</div></li>
		<li><img src="http://lorempixel.com/580/250/nature/4"> <!-- random image -->
			<div class="caption left-align">
				<h3>Поиск и оценка места захоронения</h3>
				<h5 class="light grey-text text-lighten-3">Here's our small
					slogan.</h5>
			</div></li>
	</ul>
</div>