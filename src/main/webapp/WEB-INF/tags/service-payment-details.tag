<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@tag pageEncoding="UTF-8"%>
<%@ taglib prefix="custom" tagdir="/WEB-INF/tags"%>


<custom:parallax-container />

<c:forEach var="category" items="${categories}">
	<c:if test="${not empty services[category.id]}">
		<div class="section white">
			<div class="row container">
				<div class="col l9">

					<p>Тарифи на послуги з догляду за могилами у Львові/Львівській
						області та інших регіонах України.</p>
					<p>Прейскурант цін та перелік послуг діє з 01.01.2016 по
						31.12.2016.</p>
	
					<div class="col s12">
						<table class="bordered highlight">
							<thead>
								<tr>
									<th data-field="id">Service</th>
									<th data-field="name">Description</th>
									<th data-field="price">Price</th>
								</tr>
							</thead>

							<tbody>
								<c:forEach items="${services[category.id] }" var="service">
									<tr>
										<td><c:out value="${service.serviceDatas[locale].name }" /></td>
										<td><c:out value="${service.serviceDatas[locale].description }" /></td>
										<td><c:out value="${service.price}" /></td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</div>
				<div class="col l3">Замовлення послуг</div>
			</div>
		</div>

		<custom:parallax-container />
	</c:if>
</c:forEach>



