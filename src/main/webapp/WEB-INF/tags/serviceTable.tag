<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="custom" tagdir="/WEB-INF/tags"%>
<%@tag pageEncoding="UTF-8"%>


<table class="bordered bordereded highlight">
	<thead>
		<tr>
			<th data-field="id" data-sort="int"><spring:message code="page.admin.table.service.id" /></th>
			<th data-field="name" data-sort="string"><spring:message code="page.admin.table.service.name" /></th>
			<th data-field="shortDescription" data-sort="string"><spring:message code="page.admin.table.service.shortDescription" /></th>
			<th data-field="description" data-sort="string"><spring:message code="page.admin.table.service.fullDescription" /></th>
			<th data-field="price" data-sort="float"><spring:message code="page.admin.table.service.price" /></th>
			<th data-field="category" data-sort="string"><spring:message code="page.admin.table.service.category" /></th>
			<th data-field="edit"></th>
			<th data-field="delete"></th>
		</tr>
	</thead>

	<tbody>
		<c:forEach var="service" items="${services}">
			<tr>
				<td><c:out value="${service.id}" /></td>
				<td><c:out value="${service.getServiceData(dataLocale).name}" /></td>
				<td><c:out
						value="${service.getServiceData(dataLocale).shortDescription}" /></td>
				<td><c:out
						value="${service.getServiceData(dataLocale).description}" /></td>
				<td><c:out value="${service.price}" /></td>
				<td><c:out value="${service.category.names[dataLocale].value}" /></td>
				<td><a class="modal-trigger" href="#editServiceModal"
					onclick="openEditServiceModal(${service.id});"><i
						class="material-icons">mode_edit</i></a></td>
				<td><a class="modal-trigger" href="#deleteServiceModal"
					onclick="openDeleteServiceModal(${service.id});"><i
						class="material-icons">delete</i></a></td>
			</tr>
		</c:forEach>
	</tbody>
</table>
