<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@tag pageEncoding="UTF-8"%>

<div id="addReportModal" class="modal modal-fixed-footer">
	<div class="modal-content">
		<form:form id="addReportModalform" commandName="report" action="/admin/report" method="post" enctype="multipart/form-data"
			 cssClass="add-service-form">
			<div class="modal-content">
				<h4><spring:message code="page.admin.form.report.message.addReport"/></h4>

				<div class="input-field col s4">
					<i class="material-icons prefix">web</i>
					<form:input id="customer_ru" path="reportDatas['ru'].customer" />
					<form:errors path="reportDatas['ru'].customer" />
					<label for="customer_ru"><spring:message code="page.admin.form.report.customer" arguments="ru"/></label>
				</div>

				<div class="input-field col s4">
					<i class="material-icons prefix">web</i>
					<form:input id="customer_ua" path="reportDatas['ua'].customer" />
					<form:errors path="reportDatas['ua'].customer" />
					<label for="customer_ua"><spring:message code="page.admin.form.report.customer" arguments="ua"/></label>
				</div>

				<div class="input-field col s4">
					<i class="material-icons prefix">web</i>
					<form:input id="customer_us" path="reportDatas['us'].customer" />
					<form:errors path="reportDatas['us'].customer" />
					<label for="customer_us"><spring:message code="page.admin.form.report.customer" arguments="us"/></label>
				</div>

				<div class="input-field col s4">
					<i class="material-icons prefix">web</i>
					<form:input id="deceasedPerson_ru" path="reportDatas['ru'].deceasedPerson" />
					<form:errors path="reportDatas['ru'].deceasedPerson" />
					<label for="deceasedPerson_ru"><spring:message code="page.admin.form.report.deceasedPerson" arguments="ru"/></label>
				</div>

				<div class="input-field col s4">
					<i class="material-icons prefix">web</i>
					<form:input id="deceasedPerson_ua" path="reportDatas['ua'].deceasedPerson" />
					<form:errors path="reportDatas['ua'].deceasedPerson" />
					<label for="deceasedPerson_ua"><spring:message code="page.admin.form.report.deceasedPerson" arguments="ua"/></label>
				</div>

				<div class="input-field col s4">
					<i class="material-icons prefix">web</i>
					<form:input id="deceasedPerson_us" path="reportDatas['us'].deceasedPerson" />
					<form:errors path="reportDatas['us'].deceasedPerson" />
					<label for="deceasedPerson_us"><spring:message code="page.admin.form.report.deceasedPerson" arguments="us"/></label>
				</div>

				<div class="input-field col s4">
					<i class="material-icons prefix">web</i>
					<form:input id="location_ru" path="reportDatas['ru'].location" />
					<form:errors path="reportDatas['ru'].location" />
					<label for="location_ru"><spring:message code="page.admin.form.report.location" arguments="ru"/></label>
				</div>

				<div class="input-field col s4">
					<i class="material-icons prefix">web</i>
					<form:input id="location_ua" path="reportDatas['ua'].location" />
					<form:errors path="reportDatas['ua'].location" />
					<label for="location_ua"><spring:message code="page.admin.form.report.location" arguments="ua"/></label>
				</div>

				<div class="input-field col s4">
					<i class="material-icons prefix">web</i>
					<form:input id="location_us" path="reportDatas['us'].location" />
					<form:errors path="reportDatas['us'].location" />
					<label for="location_us"><spring:message code="page.admin.form.report.location" arguments="us"/></label>
				</div>

				<div class="input-field col s4">
					<i class="material-icons prefix">web</i>
					<form:input id="cemetery_ru" path="reportDatas['ru'].cemetery" />
					<form:errors path="reportDatas['ru'].cemetery" />
					<label for="cemetery_ru"><spring:message code="page.admin.form.report.cemetery" arguments="ru"/></label>
				</div>

				<div class="input-field col s4">
					<i class="material-icons prefix">web</i>
					<form:input id="cemetery_ua" path="reportDatas['ua'].cemetery" />
					<form:errors path="reportDatas['ua'].cemetery" />
					<label for="cemetery_ua"><spring:message code="page.admin.form.report.cemetery" arguments="ua"/></label>
				</div>

				<div class="input-field col s4">
					<i class="material-icons prefix">web</i>
					<form:input id="cemetery_us" path="reportDatas['us'].cemetery" />
					<form:errors path="reportDatas['us'].cemetery" />
					<label for="cemetery_us"><spring:message code="page.admin.form.report.cemetery" arguments="us"/></label>
				</div>
				<div class="input-field col s4">
					<i class="material-icons prefix">web</i>
					<form:input id="completedWorks_ru" path="reportDatas['ru'].completedWorks" />
					<form:errors path="reportDatas['ru'].completedWorks" />
					<label for="completedWorks_ru"><spring:message code="page.admin.form.report.completedWorks" arguments="ru"/></label>
				</div>

				<div class="input-field col s4">
					<i class="material-icons prefix">web</i>
					<form:input id="completedWorks_ua" path="reportDatas['ua'].completedWorks" />
					<form:errors path="reportDatas['ua'].completedWorks" />
					<label for="completedWorks_ua"><spring:message code="page.admin.form.report.completedWorks" arguments="ua"/></label>
				</div>

				<div class="input-field col s4">
					<i class="material-icons prefix">web</i>
					<form:input id="completedWorks_us" path="reportDatas['us'].completedWorks" />
					<form:errors path="reportDatas['us'].completedWorks" />
					<label for="completedWorks_us"><spring:message code="page.admin.form.report.completedWorks" arguments="us"/></label>
				</div>

				<div class="input-field col s6">
					<i class="material-icons">today</i> <spring:message code="page.admin.form.report.orderDate"/>
					<form:input type="date" path="orderDate" cssClass="datepicker" />
				</div>

				<div class="input-field col s6">
					<i class="material-icons">today</i><spring:message code="page.admin.form.report.dueDate"/>
					<form:input type="date" path="dueDate" cssClass="datepicker" />
				</div>

				<div class="file-field input-field col s6">
					<div class="btn">
						<span><spring:message code="page.admin.form.report.files.before" /></span> <input type="file" multiple
							name="filesBefore">
					</div>
					<div class="file-path-wrapper">
						<input class="file-path validate" type="text"
							placeholder="Upload one or more files">
					</div>
				</div>

				<div class="file-field input-field col s6">
					<div class="btn">
						<span><spring:message code="page.admin.form.report.files.after" /></span> <input type="file" multiple
							name="filesAfter">
					</div>
					<div class="file-path-wrapper">
						<input class="file-path validate" type="text"
							placeholder="Upload one or more files">
					</div>
				</div>

			</div>
			<div class="col s12">
				<button class="modal-action waves-effect waves-light btn"
					type="submit" style="float: right; margin: 10px;" name="action"><spring:message code="page.admin.form.report.button.create" /></button>
				<a href="#"
					class="modal-action modal-close waves-effect waves-light red btn"
					style="float: right; margin: 10px;"><spring:message code="page.admin.form.report.button.cancel" /></a>
			</div>
		</form:form>

	</div>
</div>