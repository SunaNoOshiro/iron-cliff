<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@tag pageEncoding="UTF-8"%>

<div id="addServiceModal" class="modal modal-fixed-footer">
	<div class="modal-content">
		<form:form commandName="service" action="/admin/service" method="post" id="addServiceModalform"
			cssClass="add-service-form">
			<div class="modal-content">
				<h4><spring:message code="page.admin.form.service.message.addService"/></h4>

				<div class="input-field col s4">
					<i class="material-icons prefix">web</i>
					<form:input id="name_ru" path="serviceDatas['ru'].name" />
					<form:errors path="serviceDatas['ru'].name" />
					<label for="name_ru"><spring:message code="page.admin.form.service.placeholder.name" arguments="ru"/></label>
				</div>
				<div class="input-field col s4">
					<i class="material-icons prefix">web</i>
					<form:input id="name_ua" path="serviceDatas['ua'].name" />
					<form:errors path="serviceDatas['ua'].name" />
					<label for="name_ua"><spring:message code="page.admin.form.service.placeholder.name" arguments="ua"/></label>
				</div>
				<div class="input-field col s4">
					<i class="material-icons prefix">web</i>
					<form:input id="name_us" path="serviceDatas['us'].name" />
					<form:errors path="serviceDatas['us'].name" />
					<label for="name_us"><spring:message code="page.admin.form.service.placeholder.name" arguments="us"/></label>
				</div>

				<div class="input-field col s4">
					<i class="material-icons prefix">mode_edit</i>
					<form:input id="shortDescriptions_ru"
						path="serviceDatas['ru'].shortDescription" />
					<form:errors path="serviceDatas['ru'].shortDescription" />
					<label for="shortDescriptions_ru"><spring:message code="page.admin.form.service.placeholder.shortDescription" arguments="ru"/></label>
				</div>
				<div class="input-field col s4">
					<i class="material-icons prefix">mode_edit</i>
					<form:input id="shortDescriptions_ua"
						path="serviceDatas['ua'].shortDescription" />
					<form:errors path="serviceDatas['ua'].shortDescription" />
					<label for="shortDescriptions_ua"><spring:message code="page.admin.form.service.placeholder.shortDescription" arguments="ua"/></label>
				</div>
				<div class="input-field col s4">
					<i class="material-icons prefix">mode_edit</i>
					<form:input id="shortDescriptions_us"
						path="serviceDatas['us'].shortDescription" />
					<form:errors path="serviceDatas['us'].shortDescription" />
					<label for="shortDescriptions_us"><spring:message code="page.admin.form.service.placeholder.shortDescription" arguments="us"/></label>
				</div>

				<div class="row">
					<div class="input-field col s6">
						<i class="material-icons prefix">payment</i>
						<form:input id="price" type="number" path="price" />
						<form:errors path="price" />
						<label for="price"><spring:message code="page.admin.form.service.placeholder.price"/></label>
					</div>

					<div class="input-field col s6">
						<form:select path="category">
							<spring:message code="page.admin.form.service.select.category" var="chooseCategory"/>
							<form:option value='${chooseCategory}' />
							<form:options items="${categories}" itemValue="id"
								itemLabel="names['${dataLocale}'].value" />
						</form:select>
						<form:errors path="category" cssClass="error" />
					</div>
				</div>

<!-- 				<div class="col s12"> -->
<!-- 					<ul class="tabs"> -->
<!-- 						<li class="tab col s3"><a class="active" href="#uaeditor">Services</a></li> -->
<!-- 						<li class="tab col s3"><a href="#useditor">Reports</a></li> -->
<!-- 						<li class="tab col s3"><a href="#rueditor">Tickets</a></li> -->
<!-- 					</ul> -->
<!-- 				</div> -->
<!-- 				<div id="uaeditor" class="col s12"> -->
<!-- 					<div class="editor"></div> -->
<!-- 				</div> -->
<!-- 				<div id="useditor" class="col s12"> -->
<!-- 					<div class="editor"></div> -->

<!-- 				</div> -->
<!-- 				<div id="rueditor" class="col s12"> -->
<!-- 					<div class="editor"></div> -->
<!-- 				</div> -->



				<div class="input-field col s4">
					<i class="material-icons prefix">mode_edit</i>
					<form:textarea rows="10" cols="45" path="serviceDatas['ru'].description"
						id="description_ru" class="materialize-textarea" />
					<label for="description_ru"><spring:message code="page.admin.form.service.placeholder.fullDescription" arguments="ru"/></label>
				</div>
				
				<div class="input-field col s4">
					<i class="material-icons prefix">mode_edit</i>
					<form:textarea rows="10" cols="45" path="serviceDatas['ua'].description"
						id="description_ua" class="materialize-textarea" />
					<label for="description_ua"><spring:message code="page.admin.form.service.placeholder.fullDescription" arguments="ua"/></label>
				</div>

				<div class="input-field col s4">
					<i class="material-icons prefix">mode_edit</i>
					<form:textarea rows="10" cols="45" path="serviceDatas['us'].description"
						id="description_us" class="materialize-textarea" />
					<label for="description_us"><spring:message code="page.admin.form.service.placeholder.fullDescription" arguments="us"/></label>
				</div>
			</div>
			<div class="col s12">
				<button class="modal-action waves-effect waves-light btn"
					type="submit" style="float: right; margin: 10px;" name="action"><spring:message code="page.admin.form.service.button.create"/></button>
				<a href="#"
					class="modal-action modal-close waves-effect waves-light red btn"
					style="float: right; margin: 10px;"><spring:message code="page.admin.form.service.button.cancel"/></a>
			</div>
		</form:form>
	</div>
</div>