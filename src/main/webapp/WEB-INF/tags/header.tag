<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@tag pageEncoding="UTF-8"%>

<ul id="dropdown1" class="dropdown-content">
	<li><a href="?locale=ua" onclick="changeLocale('ua')">UA</a></li>
	<li class="divider"></li>
	<li><a href="?locale=ru" onclick="changeLocale('ru')">RU</a></li>
	<li class="divider"></li>
	<li><a href="?locale=pl" onclick="changeLocale('pl')">PL</a></li>
	<li class="divider"></li>
	<li><a href="?locale=us" onclick="changeLocale('us')">US</a></li>
</ul>

<div class="navbar-fixed">
	<nav>
		<div class="nav-wrapper ">
			<a href="#" id="logoNav" class="brand-logo">IRON CLIFF</a>
			<ul id="nav-mobile" class="right hide-on-med-and-down">
				<li><a class="dropdown-button" href="#"
					data-activates="dropdown1"><spring:message
							code="page.header.link.changeLanguage" /><i
						class="material-icons right">arrow_drop_down</i></a></li>
				<li><a href="${homePage}" id="homeNav"><spring:message
							code="page.header.link.home" /></a></li>
				<li><a href="${servicesPage}" id="serviceNav"><spring:message
							code="page.header.link.services" /></a></li>
				<li><a href="${reportsPage}" id="reportNav"><spring:message
							code="page.header.link.reports" /></a></li>
				<li><a href="${contactPage}" id="contactNav"><spring:message
							code="page.header.link.contact" /></a></li>
			</ul>
		</div>
	</nav>
</div>