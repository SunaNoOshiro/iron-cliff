<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<%@ taglib prefix="custom" tagdir="/WEB-INF/tags"%>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<title>Services</title>

<link href="/resources/css/materialize.css" rel="stylesheet" />
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">
<link href="/resources/css/style.css" rel="stylesheet">

<!-- Mainly scripts -->
<script src="/resources/js/jquery-2.1.1.min.js"></script>
<script src="/resources/js/jquery.validate-1.14.0.min.js"></script>
<script src="/resources/js/materialize.min.js"></script>

</head>

<body>
	<custom:header />
		<custom:service-payment-details  />
	<custom:footer />

	<script type="text/javascript">
		$(document).ready(function() {
			$('.parallax').parallax();
		});
	</script>
</body>
</html>
