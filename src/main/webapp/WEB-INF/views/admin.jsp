<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="custom" tagdir="/WEB-INF/tags"%>

<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<title><spring:message code="page.admin.title" /></title>

<link href="/resources/css/materialize.css" rel="stylesheet" />
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">
<link href="/resources/css/style.css" rel="stylesheet">
<!-- <link href="/resources/css/materialNote.css" rel="stylesheet"> -->


<!-- Mainly scripts -->
<script src="/resources/js/jquery-2.1.1.min.js"></script>
<script src="/resources/js/jquery.validate-1.14.0.min.js"></script>
<script src="/resources/js/materialize.min.js"></script>
<script src="/resources/js/materialNote.js"></script>
<script src="/resources/js/ckMaterializeOverrides.js"></script>
<script src="/resources/js/main.js"></script>
<script src="/resources/js/stupidtable.js"></script>
</head>

<body>

	<style>
.modal {
	top: 0 !important;
	margin-left: 0px;
	margin-right: 0px;
	width: 100%;
	height: 100%;
	max-height: 100%;
	overflow: hidden;
}

.modal.modal-fixed-footer {
	padding: 0;
	height: 100%;
}

#deleteServiceModal, #deleteReportModal {
	height: 40%;
	width: 60%;
	left: 20%;
	top: 25% !important;
}

.materialize-textarea {
	max-height: 400px;
}

.modal.modal-fixed-footer .modal-content {
	position: relative;
	width: 100%;
	overflow-y: visible;
}

.file-field {
	display: inline-block;
	width: 100%;
}

input#dueDate {
	width: 70%;
	margin-left: 10%;
}

input#orderDate {
	width: 70%;
	margin-left: 10%;
}
</style>
	<custom:header />

	<div class="row">
		<div class="col s12">
			<ul class="tabs">
				<li class="tab col s6"><a
					class="<c:if test='${tab eq "SERVICES"}'>active</c:if>"
					href="#services"><spring:message code="page.admin.tab.services" /></a></li>
				<li class="tab col s6"><a
					class="<c:if test='${tab eq "REPORTS"}'>active</c:if>"
					href="#reports"><spring:message code="page.admin.tab.reports" /></a></li>
			</ul>
		</div>
		<div id="services" class="col s12">
			<div class="col s12">
				<custom:addServiceModal />
			</div>

			<div class="col s6">
				<a class="waves-effect waves-light btn modal-trigger"
					href="#addServiceModal"><spring:message
						code="page.admin.button.addService" /></a>
			</div>

			<custom:serviceTable />

			<custom:deleteServiceModal />

			<custom:editServiceModal />

		</div>
		<div id="reports" class="col s12">

			<a class="waves-effect waves-light btn modal-trigger"
				href="#addReportModal"><spring:message code="page.admin.button.addReport" /></a>

			<div class="row">
				<custom:addReportModal />
			</div>

			<custom:reportTable />

			<custom:editReportModal />

			<custom:deleteReportModal />

		</div>
	</div>
	<script>
		function openEditServiceModal(id) {
			$.ajax({
				url : "/admin/service/" + id,
				method : "GET"

			}).done(
					function(data) {
						$("#editServiceModal #name_ru").val(
								data.serviceDatas['ru'].name);
						$("#editServiceModal #name_ua").val(
								data.serviceDatas['ua'].name);
						$("#editServiceModal #name_us").val(
								data.serviceDatas['us'].name);

						$("#editServiceModal #description_ru").val(
								data.serviceDatas['ru'].description);
						$("#editServiceModal #description_ua").val(
								data.serviceDatas['ua'].description);
						$("#editServiceModal #description_us").val(
								data.serviceDatas['us'].description);

						$("#editServiceModal #shortDescriptions_ru").val(
								data.serviceDatas['ru'].shortDescription);
						$("#editServiceModal #shortDescriptions_ua").val(
								data.serviceDatas['ua'].shortDescription);
						$("#editServiceModal #shortDescriptions_us").val(
								data.serviceDatas['us'].shortDescription);

						$("#editServiceModal #price").val(data.price);
						$("#editServiceModal #category").val(
								data.category.names['${dataLocale}'].value);

						serviceToEditId = id;

						$("#editServiceModalform").attr("action",
								"/admin/service/" + data.id);
						$("#editServiceModal label").attr("class", "active");
					});
		}

		function openEditReportModal(id) {
			$.ajax({
				url : "/admin/report/" + id,
				method : "GET"

			}).done(
					function(data) {

						$("#editReportModal #customer_ru").val(
								data.reportDatas['ru'].customer);
						$("#editReportModal #customer_ua").val(
								data.reportDatas['ua'].customer);
						$("#editReportModal #customer_us").val(
								data.reportDatas['us'].customer);

						$("#editReportModal #deceasedPerson_ru").val(
								data.reportDatas['ru'].deceasedPerson);
						$("#editReportModal #deceasedPerson_ua").val(
								data.reportDatas['ua'].deceasedPerson);
						$("#editReportModal #deceasedPerson_us").val(
								data.reportDatas['us'].deceasedPerson);

						$("#editReportModal #location_ru").val(
								data.reportDatas['ru'].location);
						$("#editReportModal #location_ua").val(
								data.reportDatas['ua'].location);
						$("#editReportModal #location_us").val(
								data.reportDatas['us'].location);

						$("#editReportModal #cemetery_ru").val(
								data.reportDatas['ru'].cemetery);
						$("#editReportModal #cemetery_ua").val(
								data.reportDatas['ua'].cemetery);
						$("#editReportModal #cemetery_us").val(
								data.reportDatas['us'].cemetery);

						$("#editReportModal #completedWorks_ru").val(
								data.reportDatas['ru'].completedWorks);
						$("#editReportModal #completedWorks_ua").val(
								data.reportDatas['ua'].completedWorks);
						$("#editReportModal #lcompletedWorks_us").val(
								data.reportDatas['us'].completedWorks);

						$("#editReportModal #orderDate").val(data.orderDate);
						$("#editReportModal #dueDate").val(data.dueDate);

						$("#editReportModalform").attr("action",
								"/admin/reports/" + data.id);
						$("#editReportModal label").attr("class", "active");

						reportToEditId = id;
					});
		}
		function openDeleteServiceModal(id) {
			$.ajax({
				url : "/admin/service/" + id,
				type : "GET"
			}).done(
					function(data) {

						if (data.serviceDatas['${dataLocale}'] !== undefined
								|| data.serviceDatas['${dataLocale}'] != null) {
							$("#serviceToDelete").html(
									data.serviceDatas['${dataLocale}'].name);
						}

						$("#deleteServiceModalform").attr("action",
								"/admin/service/" + data.id);
						$("#serviceToDeleteID").html(id);
					});

		}

		function openDeleteReportModal(id) {
			$.ajax({
				url : "/admin/report/" + id,
				type : "GET"
			}).done(
					function(data) {

						$("#reportToDelete").html(
								data.reportDatas['${dataLocale}'].customer
										+ data.orderDate);

						$("#deleteReportModalform").attr("action",
								"/admin/report/" + data.id);
						$("#reportToDeleteID").html(id);
					});
		}

		$(document).ready(function() {
			$("#addServiceModalform").validate({
				errorElement : 'span',
				errorClass : 'error red-text text-darken-4 invalid',
				validClass : "valid",
				rules : {
					"serviceDatas['ua'].name" : {
						required : true,
						minlength : 6
					},
					"serviceDatas['us'].name" : {
						required : true,
						minlength : 6
					},
					"serviceDatas['ru'].name" : {
						required : true,
						minlength : 6
					},
					"serviceDatas['ua'].shortDescription" : {
						required : true,
						minlength : 6
					},
					"serviceDatas['us'].shortDescription" : {
						required : true,
						minlength : 6
					},
					"serviceDatas['ru'].shortDescription" : {
						required : true,
						minlength : 6
					},
					"serviceDatas['ua'].description" : {
						required : true,
						minlength : 6
					},
					"serviceDatas['us'].description" : {
						required : true,
						minlength : 6
					},
					"serviceDatas['ru'].description" : {
						required : true,
						minlength : 6
					},
					price : {
						required : true
					},
					category : {
						required : true
					}
				}
			});
			$("#editServiceModalform").validate({
				errorElement : 'span',
				errorClass : 'error red-text text-darken-4 invalid',
				validClass : "valid",
				rules : {
					"serviceDatas['ua'].name" : {
						required : true,
						minlength : 6
					},
					"serviceDatas['us'].name" : {
						required : true,
						minlength : 6
					},
					"serviceDatas['ru'].name" : {
						required : true,
						minlength : 6
					},
					"serviceDatas['ua'].shortDescription" : {
						required : true,
						minlength : 6
					},
					"serviceDatas['us'].shortDescription" : {
						required : true,
						minlength : 6
					},
					"serviceDatas['ru'].shortDescription" : {
						required : true,
						minlength : 6
					},
					"serviceDatas['ua'].description" : {
						required : true,
						minlength : 6
					},
					"serviceDatas['us'].description" : {
						required : true,
						minlength : 6
					},
					"serviceDatas['ru'].description" : {
						required : true,
						minlength : 6
					},
					price : {
						required : true
					},
					category : {
						required : true
					}
				}
			});
			$("#addReportModalform").validate({
				errorElement : 'span',
				errorClass : 'error red-text text-darken-4 invalid',
				validClass : "valid",
				rules : {
					"reportDatas['ua'].customer" : {
						required : true,
						minlength : 6
					},
					"reportDatas['us'].customer" : {
						required : true,
						minlength : 6
					},
					"reportDatas['ru'].customer" : {
						required : true,
						minlength : 6
					},
					"reportDatas['ua'].deceasedPerson" : {
						required : true,
						minlength : 6
					},
					"reportDatas['us'].deceasedPerson" : {
						required : true,
						minlength : 6
					},
					"reportDatas['ru'].deceasedPerson" : {
						required : true,
						minlength : 6
					},
					"reportDatas['ua'].location" : {
						required : true,
						minlength : 3
					},
					"reportDatas['us'].location" : {
						required : true,
						minlength : 3
					},
					"reportDatas['ru'].location" : {
						required : true,
						minlength : 3
					},
					"reportDatas['ua'].cemetery" : {
						required : true,
						minlength : 6
					},
					"reportDatas['us'].cemetery" : {
						required : true,
						minlength : 6
					},
					"reportDatas['ru'].cemetery" : {
						required : true,
						minlength : 6
					},
					"reportDatas['ua'].completedWorks" : {
						required : true,
						minlength : 6
					},
					"reportDatas['us'].completedWorks" : {
						required : true,
						minlength : 6
					},
					"reportDatas['ru'].completedWorks" : {
						required : true,
						minlength : 6
					},
					orderDate : {
						required : true
					},
					dueDate : {
						required : true
					}
				}
			});
			$("#editReportModalform").validate({
				errorElement : 'span',
				errorClass : 'error red-text text-darken-4 invalid',
				validClass : "valid",
				rules : {
					"reportDatas['ua'].customer" : {
						required : true,
						minlength : 6
					},
					"reportDatas['us'].customer" : {
						required : true,
						minlength : 6
					},
					"reportDatas['ru'].customer" : {
						required : true,
						minlength : 6
					},
					"reportDatas['ua'].deceasedPerson" : {
						required : true,
						minlength : 6
					},
					"reportDatas['us'].deceasedPerson" : {
						required : true,
						minlength : 6
					},
					"reportDatas['ru'].deceasedPerson" : {
						required : true,
						minlength : 6
					},
					"reportDatas['ua'].location" : {
						required : true,
						minlength : 3
					},
					"reportDatas['us'].location" : {
						required : true,
						minlength : 3
					},
					"reportDatas['ru'].location" : {
						required : true,
						minlength : 3
					},
					"reportDatas['ua'].cemetery" : {
						required : true,
						minlength : 6
					},
					"reportDatas['us'].cemetery" : {
						required : true,
						minlength : 6
					},
					"reportDatas['ru'].cemetery" : {
						required : true,
						minlength : 6
					},
					"reportDatas['ua'].completedWorks" : {
						required : true,
						minlength : 6
					},
					"reportDatas['us'].completedWorks" : {
						required : true,
						minlength : 6
					},
					"reportDatas['ru'].completedWorks" : {
						required : true,
						minlength : 6
					},
					orderDate : {
						required : true
					},
					dueDate : {
						required : true
					}
				}
			});
		});
	</script>
	<custom:scripts></custom:scripts>
</body>
</html>
