package com.suna.ironcliff.utils;

public class WebConstants {

	public static final String CONTACT = "/contact";
	public static final String SERVICES = "/services";
	public static final String REPORTS = "/reports";
	public static final String ADMIN = "/admin";
	public static final String HOME = "/home";

	public static String getHomePagePath() {
		return HOME;
	}

	public static String getAdminPagePath() {
		return ADMIN;
	}

	public String getReportsPagePath() {
		return REPORTS;
	}

	public String getServicesPagePath() {
		return SERVICES;
	}

	public String getContactPagePath() {
		return CONTACT;
	}
}
