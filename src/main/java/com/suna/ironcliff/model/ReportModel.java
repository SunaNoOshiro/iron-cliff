package com.suna.ironcliff.model;

import java.sql.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

@Entity(name = "report")
public class ReportModel {

	@Id
	@GeneratedValue
	private Long id;

	@Column
	private Date orderDate;

	@Column
	private Date dueDate;

	@ManyToMany(fetch = FetchType.EAGER)
	private Map<String, ReportDataModel> reportDatas;

	@OneToMany(fetch = FetchType.EAGER)
	private List<FileModel> files;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Map<String, ReportDataModel> getReportDatas() {
		return reportDatas;
	}

	public void setReportDatas(Map<String, ReportDataModel> reportDatas) {
		this.reportDatas = reportDatas;
	}

	public ReportDataModel getReportData(String local) {
		return reportDatas.get(local);
	}

	public Date getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}

	public Date getDueDate() {
		return dueDate;
	}

	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}

	public List<FileModel> getFiles() {
		return files;
	}

	public void setFiles(List<FileModel> files) {
		this.files = files;
	}
}
