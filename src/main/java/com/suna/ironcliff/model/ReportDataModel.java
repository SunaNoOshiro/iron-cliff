package com.suna.ironcliff.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity(name = "report_data")
public class ReportDataModel {

	@Id
	@GeneratedValue
	private Long id;

	@Column
	private String customer;

	@Column
	private String deceasedPerson;

	@Column
	private String location;

	@Column
	private String cemetery;

	@Column
	private String completedWorks;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCustomer() {
		return customer;
	}

	public void setCustomer(String customer) {
		this.customer = customer;
	}

	public String getDeceasedPerson() {
		return deceasedPerson;
	}

	public void setDeceasedPerson(String deceasedPerson) {
		this.deceasedPerson = deceasedPerson;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getCemetery() {
		return cemetery;
	}

	public void setCemetery(String cemetery) {
		this.cemetery = cemetery;
	}

	public String getCompletedWorks() {
		return completedWorks;
	}

	public void setCompletedWorks(String completedWorks) {
		this.completedWorks = completedWorks;
	}

}
