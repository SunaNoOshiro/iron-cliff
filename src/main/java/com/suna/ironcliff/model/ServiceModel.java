package com.suna.ironcliff.model;

import java.math.BigDecimal;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;

@Entity(name = "service")
public class ServiceModel {

	@Id
	@GeneratedValue
	private Long id;

	@ManyToMany(fetch = FetchType.EAGER)
	private Map<String, ServiceDataModel> serviceDatas;

	@Column
	private BigDecimal price;

	@OneToOne
	private CategoryModel category;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public CategoryModel getCategory() {
		return category;
	}

	public void setCategory(CategoryModel category) {
		this.category = category;
	}

	public Map<String, ServiceDataModel> getServiceDatas() {
		return serviceDatas;
	}

	public void setServiceDatas(Map<String, ServiceDataModel> serviceDatas) {
		this.serviceDatas = serviceDatas;
	}

	public ServiceDataModel getServiceData(String locale) {
		return serviceDatas.get(locale);
	}

}
