package com.suna.ironcliff.model;

import java.util.Map;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

@Entity(name = "category")
public class CategoryModel {

	@Id
	@GeneratedValue
	private Long id;

	@ManyToMany(fetch = FetchType.EAGER)
	private Map<String, NameModel> names;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Map<String, NameModel> getNames() {
		return names;
	}

	public void setNames(Map<String, NameModel> names) {
		this.names = names;
	}
}
