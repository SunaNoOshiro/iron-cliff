package com.suna.ironcliff.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

import com.suna.ironcliff.enums.RoleEnum;

@Entity(name = "role")
public class RoleModel {
	@Id
	@GeneratedValue
	private Long id;

	@Enumerated(EnumType.STRING)
	private RoleEnum value;

	@ManyToMany(mappedBy = "roles")
	private List<UserModel> users;

	public List<UserModel> getUsers() {
		return users;
	}

	public void setUsers(List<UserModel> users) {
		this.users = users;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public RoleEnum getValue() {
		return value;
	}

	public void setValue(RoleEnum value) {
		this.value = value;
	}

}
