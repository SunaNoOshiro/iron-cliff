package com.suna.ironcliff;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import com.suna.ironcliff.enums.RoleEnum;
import com.suna.ironcliff.model.RoleModel;
import com.suna.ironcliff.model.UserModel;
import com.suna.ironcliff.repository.RoleRepository;
import com.suna.ironcliff.repository.UserRepository;

@Component
public class DBInit implements ApplicationListener<ContextRefreshedEvent> {

	private static Logger LOG  = LoggerFactory.getLogger(DBInit.class);
	
	@Autowired
	private UserRepository userRepository;

	@Autowired
	private RoleRepository roleRepository;

	@Override
	public void onApplicationEvent(final ContextRefreshedEvent event) {
		if (roleRepository.findByValue(RoleEnum.ROLE_ADMIN).isEmpty()) {
			RoleModel adminRole = new RoleModel();
			adminRole.setValue(RoleEnum.ROLE_ADMIN);
			roleRepository.save(adminRole);
			LOG.info("Creating ROLE_ADMIN");
		}

		if (userRepository.findByLogin("admin").isEmpty()) {
			UserModel user = new UserModel();
			user.setLogin("admin");
			user.setPassword(new BCryptPasswordEncoder().encode("adminnimda"));
			user.setRoles(roleRepository.findAll());
			userRepository.save(user);
			LOG.info("Creating admin user");
		}
		
	}
}
