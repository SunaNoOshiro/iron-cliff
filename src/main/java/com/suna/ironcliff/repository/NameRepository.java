package com.suna.ironcliff.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.suna.ironcliff.model.NameModel;

public interface NameRepository extends JpaRepository<NameModel, Long> {
}
