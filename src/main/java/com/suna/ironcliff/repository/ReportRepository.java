package com.suna.ironcliff.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.suna.ironcliff.model.ReportModel;

public interface ReportRepository extends JpaRepository<ReportModel, Long> {
}
