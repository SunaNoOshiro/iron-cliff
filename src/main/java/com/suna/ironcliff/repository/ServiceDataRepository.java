package com.suna.ironcliff.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.suna.ironcliff.model.ServiceDataModel;

public interface ServiceDataRepository extends JpaRepository<ServiceDataModel, Long> {
}
