package com.suna.ironcliff.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.suna.ironcliff.enums.RoleEnum;
import com.suna.ironcliff.model.RoleModel;

public interface RoleRepository extends JpaRepository<RoleModel, Long> {
	List<RoleModel> findByValue(RoleEnum value);
}
