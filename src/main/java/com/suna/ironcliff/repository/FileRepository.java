package com.suna.ironcliff.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.suna.ironcliff.model.FileModel;

public interface FileRepository extends JpaRepository<FileModel, Long> {
}
