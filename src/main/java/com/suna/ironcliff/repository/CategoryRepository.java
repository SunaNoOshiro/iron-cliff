package com.suna.ironcliff.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.suna.ironcliff.model.CategoryModel;

public interface CategoryRepository extends JpaRepository<CategoryModel, Long> {
}
