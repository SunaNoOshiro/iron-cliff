package com.suna.ironcliff.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.suna.ironcliff.model.ReportDataModel;

public interface ReportDataRepository extends JpaRepository<ReportDataModel, Long> {
}
