package com.suna.ironcliff.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.suna.ironcliff.model.CategoryModel;
import com.suna.ironcliff.model.ServiceModel;


public interface ServiceRepository extends JpaRepository<ServiceModel, Long> {
	
	List<ServiceModel> findByCategory(CategoryModel category);
}
