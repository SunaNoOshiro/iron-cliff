package com.suna.ironcliff.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.suna.ironcliff.model.UserModel;

public interface UserRepository extends JpaRepository<UserModel, Long> {
	List<UserModel> findByLogin(String login);
}
