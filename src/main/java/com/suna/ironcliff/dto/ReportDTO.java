package com.suna.ironcliff.dto;

import java.sql.Date;
import java.util.List;

public class ReportDTO {

	private Long id;

	private Date orderDate;

	private Date dueDate;

	private String customer;

	private String deceasedPerson;

	private String location;

	private String cemetery;

	private String completedWorks;

	private List<FileDTO> files;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}

	public Date getDueDate() {
		return dueDate;
	}

	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}

	public List<FileDTO> getFiles() {
		return files;
	}

	public void setFiles(List<FileDTO> files) {
		this.files = files;
	}

	public String getCustomer() {
		return customer;
	}

	public void setCustomer(String customer) {
		this.customer = customer;
	}

	public String getDeceasedPerson() {
		return deceasedPerson;
	}

	public void setDeceasedPerson(String deceasedPerson) {
		this.deceasedPerson = deceasedPerson;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getCemetery() {
		return cemetery;
	}

	public void setCemetery(String cemetery) {
		this.cemetery = cemetery;
	}

	public String getCompletedWorks() {
		return completedWorks;
	}

	public void setCompletedWorks(String completedWorks) {
		this.completedWorks = completedWorks;
	}

}
