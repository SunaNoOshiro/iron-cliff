package com.suna.ironcliff.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import org.springframework.ui.Model;

public class AbstractPageController {

	public static final String CONTACT = "/contact";
	public static final String SERVICES = "/services";
	public static final String REPORTS = "/reports";
	public static final String ADMIN = "/admin";
	public static final String HOME = "/home";

	protected static final String DEFAULT_LOCALE = "ua";

	protected List<String> availableLocales = Arrays.asList("ua", "us", "ru", "pl");

	public void addAllAttributesToModel(Model model, Locale locale) {
		model.addAttribute("defaultLocale", DEFAULT_LOCALE);
		model.addAttribute("homePage", HOME);
		model.addAttribute("adminPage", ADMIN);
		model.addAttribute("reportsPage", REPORTS);
		model.addAttribute("servicesPage", SERVICES);
		model.addAttribute("contactPage", CONTACT);

		if(availableLocales.contains(locale.toString()))
			model.addAttribute("dataLocale", locale.toString());
		else
			model.addAttribute("dataLocale", DEFAULT_LOCALE);
			
	}
}
