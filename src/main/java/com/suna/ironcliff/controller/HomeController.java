package com.suna.ironcliff.controller;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.suna.ironcliff.repository.ServiceRepository;

@Controller
public class HomeController extends AbstractPageController {

	@Autowired
	private ServiceRepository serviceRepository;

	@RequestMapping(value = "/home", method = RequestMethod.GET)
	public String renderHomePage(Locale locale, final Model model) {
		
		model.addAttribute("services", serviceRepository.findAll());

		addAllAttributesToModel(model,locale);

		return "home";
	}
}
