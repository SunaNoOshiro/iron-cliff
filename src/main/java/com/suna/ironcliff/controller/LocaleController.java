package com.suna.ironcliff.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/locale")
public class LocaleController extends AbstractPageController {

	@RequestMapping(method = RequestMethod.POST)
	public String changeLocale(@RequestParam("locale") String locale, @RequestParam("url") String url) {

		return "redirect:/" + locale + url;
	}
}
