package com.suna.ironcliff.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.suna.ironcliff.model.CategoryModel;
import com.suna.ironcliff.model.ServiceModel;
import com.suna.ironcliff.repository.CategoryRepository;
import com.suna.ironcliff.repository.ServiceRepository;

@Controller
@RequestMapping()
public class ServicesAndPaymentPageController extends AbstractPageController {

	@Autowired
	private ServiceRepository serviceRepository;

	@Autowired
	private CategoryRepository categoryRepository;

	@RequestMapping(value = "/services", method = RequestMethod.GET)
	private String renderServicePage(final Locale locale, final Model model) {

		Map<Long, List<ServiceModel>> map = new HashMap<>();
		List<CategoryModel> categories = categoryRepository.findAll();

		for (CategoryModel category : categories) {
			map.put(category.getId(), serviceRepository.findByCategory(category));
		}

		model.addAttribute("services", map);
		model.addAttribute("locale", locale);
		model.addAttribute("categories", categories);
		addAllAttributesToModel(model, locale);

		return "services";
	}
}
