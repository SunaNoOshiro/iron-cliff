package com.suna.ironcliff.controller;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map.Entry;
import java.util.UUID;

import javax.validation.Valid;

import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.suna.ironcliff.enums.AdminTab;
import com.suna.ironcliff.model.CategoryModel;
import com.suna.ironcliff.model.FileModel;
import com.suna.ironcliff.model.NameModel;
import com.suna.ironcliff.model.ReportModel;
import com.suna.ironcliff.model.ReportDataModel;
import com.suna.ironcliff.model.ServiceModel;
import com.suna.ironcliff.model.ServiceDataModel;
import com.suna.ironcliff.repository.CategoryRepository;
import com.suna.ironcliff.repository.FileRepository;
import com.suna.ironcliff.repository.NameRepository;
import com.suna.ironcliff.repository.ReportDataRepository;
import com.suna.ironcliff.repository.ReportRepository;
import com.suna.ironcliff.repository.ServiceDataRepository;
import com.suna.ironcliff.repository.ServiceRepository;

@Controller
public class AdminPageController extends AbstractPageController {

	@Autowired
	private ServiceRepository serviceRepository;

	@Autowired
	private ServiceDataRepository serviceDataRepository;

	@Autowired
	private NameRepository nameRepository;

	@Autowired
	private CategoryRepository categoryRepository;

	@Autowired
	private ReportRepository reportRepository;

	@Autowired
	private ReportDataRepository reportDataRepository;

	@Autowired
	private FileRepository fileRepository;

	@ModelAttribute("service")
	public ServiceModel serviceAttribute() {
		return new ServiceModel();
	}

	@ModelAttribute("report")
	public ReportModel reportAttribute() {
		ReportModel report = new ReportModel();
		report.setFiles(new ArrayList<FileModel>());
		return report;
	}

	@RequestMapping(value = "/admin", method = RequestMethod.GET)
	public String renderAdminPage(final Locale locale, final Model model) {
		populateMVCModel(locale, model, AdminTab.SERVICES);

		return "admin";
	}

	@RequestMapping(value = "/admin", params = "tab", method = RequestMethod.GET)
	public String renderAdminPage(final Locale locale, final Model model, @RequestParam("tab") String tabStr) {
		populateMVCModel(locale, model, getTab(tabStr));

		return "admin";
	}

	private void populateMVCModel(final Locale locale, final Model model, AdminTab tab) {
		model.addAttribute("categories", categoryRepository.findAll());
		model.addAttribute("services", serviceRepository.findAll());
		model.addAttribute("reports", reportRepository.findAll());
		model.addAttribute("tab", tab);
		addAllAttributesToModel(model, locale);
	}

	private AdminTab getTab(String tabStr) {
		try {
			return AdminTab.valueOf(tabStr.toUpperCase());

		} catch (Exception e) {
			return AdminTab.SERVICES;
		}
	}

	@RequestMapping(value = "/admin/category", method = RequestMethod.POST)
	public String addCategory(Model model, @Valid @ModelAttribute("category") CategoryModel category, BindingResult result) {
		if (result.hasErrors()) {
		}

		saveCategory(category);

		return "redirect:/admin";
	}

	@RequestMapping(value = "/admin/category", method = RequestMethod.PUT)
	public String editCategory(Model model, @Valid @ModelAttribute("category") CategoryModel category,
			BindingResult result) {
		if (result.hasErrors()) {
		}

		if (category.getId() != null) {
			saveCategory(category);
		}

		return "redirect:/admin";
	}

	private void saveCategory(CategoryModel category) {
		for (Entry<String, NameModel> entry : category.getNames().entrySet()) {
			nameRepository.save(entry.getValue());
		}

		categoryRepository.save(category);
	}

	@RequestMapping(value = "/admin/report/{id}", method = RequestMethod.GET)
	@ResponseBody
	public ReportModel saveReport(@PathVariable("id") Long id) {
		return reportRepository.findOne(id);
	}

	@RequestMapping(value = "/admin/report", method = RequestMethod.POST)
	public String saveReport(Model model, @Valid @ModelAttribute("report") ReportModel report, BindingResult result,
			@RequestParam("filesBefore") MultipartFile[] filesBefore,
			@RequestParam("filesAfter") MultipartFile[] filesAfter) {
		if (result.hasErrors()) {
		}
		System.out.println(report.getReportDatas().get("ua").getLocation());

		saveFiles(report, filesBefore, "Before");
		saveFiles(report, filesAfter, "After");

		saveReport(report);

		return "redirect:/admin";
	}

	@RequestMapping(value = "/admin/reports/{id}", method = RequestMethod.POST)
	public String editReport(Model model, @Valid @ModelAttribute("report") ReportModel report, BindingResult result,
			@RequestParam("filesBefore") MultipartFile[] filesBefore,
			@RequestParam("filesAfter") MultipartFile[] filesAfter, @PathVariable("id") Long id) {

		if (reportRepository.exists(id)) {
			report.setId(id);
			if (filesBefore.length > 0)
				saveFiles(report, filesBefore, "Before");

			if (filesAfter.length > 0)
				saveFiles(report, filesAfter, "After");

			saveReport(report);
		}

		return "redirect:/admin";
	}

	@RequestMapping(value = "/admin/report/{id}", method = RequestMethod.POST)
	public String deleteReport(Model model, @PathVariable("id") Long id) {
		if (reportRepository.exists(id)) {
			reportRepository.delete(id);
		}

		return "redirect:/admin";
	}

	private void saveFiles(ReportModel report, MultipartFile[] files, String title) {
		for (MultipartFile multipartFile : files) {
			String fileName = saveFileOnServer(generateFileName(multipartFile), multipartFile);

			if (fileName != null) {
				populateReport(report, multipartFile, fileName, title);
			}
			System.out.println(report.getReportDatas().get("ua").getLocation());

		}
	}

	private String generateFileName(MultipartFile multipartFile) {
		String originalFilename = multipartFile.getOriginalFilename();
		String fileName = UUID.randomUUID().toString();

		return fileName + "." + FilenameUtils.getExtension(originalFilename);
	}

	private void populateReport(ReportModel report, MultipartFile multipartFile, String fileName, String title) {
		FileModel file = new FileModel();
		file.setName(fileName);
		file.setTitle(title);
		report.getFiles().add(file);
	}

	private String saveFileOnServer(String name, MultipartFile file) {
		try {
			if (file != null && file.getBytes().length > 0) {
				String rootPath = System.getProperty("catalina.home");
				java.io.File dir = new java.io.File(rootPath + java.io.File.separator + "files");

				if (!dir.exists())
					dir.mkdirs();

				java.io.File serverFile = new java.io.File(dir.getAbsolutePath() + java.io.File.separator + name);
				BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));

				byte[] bytes = file.getBytes();

				stream.write(bytes);
				stream.close();

				return name;
			} else
				return null;

		} catch (Exception e) {
			return null;
		}
	}

	private void saveReport(ReportModel report) {
		for (Entry<String, ReportDataModel> reportEntry : report.getReportDatas().entrySet()) {
			reportDataRepository.save(reportEntry.getValue());
		}

		saveReportFiles(report.getFiles());
		System.out.println(report.getReportDatas().get("ua").getLocation());

		reportRepository.save(report);
	}

	private void saveReportFiles(List<FileModel> files) {
		for (FileModel entry : files) {
			fileRepository.save(entry);
		}
	}

	@RequestMapping(value = "/admin/service", params = { "categoryId" }, method = RequestMethod.POST)
	public String addService(Model model, @Valid @ModelAttribute("service") ServiceModel service, BindingResult result,
			@RequestParam("categoryId") Long categoryId) {
		if (result.hasErrors()) {
		}

		service.setCategory(categoryRepository.findOne(categoryId));
		saveService(service);

		return "redirect:/admin";
	}

	@RequestMapping(value = "/admin/service", method = RequestMethod.POST)
	public String addService(Model model, @Valid @ModelAttribute("service") ServiceModel service, BindingResult result) {
		if (result.hasErrors()) {
		}

		service.setCategory(categoryRepository.findOne(1L));
		saveService(service);

		return "redirect:/admin";
	}

	@RequestMapping(value = "/admin/service/{id}", params = { "categoryId" }, method = RequestMethod.POST)
	public String editService(Model model, @Valid @ModelAttribute("service") ServiceModel service, BindingResult result,
			@PathVariable("id") Long id, @RequestParam("categoryId") Long categoryId) {
		if (result.hasErrors()) {
		}

		saveUpdatedServiceIfExist(service, id, categoryId);

		return "redirect:/admin";
	}

	private void saveUpdatedServiceIfExist(ServiceModel service, Long id, Long categoryId) {
		if (serviceRepository.exists(id)) {
			service.setId(id);
			service.setCategory(categoryRepository.findOne(categoryId));
			saveService(service);
		}
	}

	@RequestMapping(value = "/admin/service/{id}", params = "id", method = RequestMethod.POST)
	public ServiceModel editService(Model model, @Valid @ModelAttribute("service") ServiceModel service, BindingResult result,
			@PathVariable("id") Long id) {
		if (result.hasErrors()) {
		}

		saveUpdatedServiceIfExist(service, id, 1L);

		return service;
	}

	@RequestMapping(value = "/admin/service/{id}", method = RequestMethod.GET)
	@ResponseBody
	public ServiceModel getService(@PathVariable("id") Long id) {
		return serviceRepository.findOne(id);
	}

	@RequestMapping(value = "/admin/service/{id}", method = RequestMethod.POST)
	public String deleteService(Model model, @PathVariable("id") Long id) {
		if (serviceRepository.exists(id)) {
			serviceRepository.delete(id);
		}

		return "redirect:/admin";
	}

	private void saveService(ServiceModel service) {
		for (Entry<String, ServiceDataModel> entry : service.getServiceDatas().entrySet()) {
			serviceDataRepository.save(entry.getValue());
		}

		if (service.getCategory() == null) {
			service.setCategory(categoryRepository.findOne(1L));
		}

		serviceRepository.save(service);
	}

}
